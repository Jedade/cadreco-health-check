let express = require('express');
const fs = require('fs');
let app = express();
const nodemailer = require('nodemailer');
const newman = require('newman');
const path = require('path');
const directory = 'newman';

let transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
      user: 'benglutenever@gmail.com', // TODO: your gmail account
      pass: 'Gbetohovehoum@7' // TODO: your gmail password
}
});

let mailOptions = {
  from: 'benglutenever@gmail.com', // TODO: email sender
  to: 'zinsoumarcellin7@gmail.com, onesime.adefoulou@gmail.com', // TODO: email receiver
  subject: 'Nodemailer - Test',
  text: "Ci-dessous les résultats du test avec les erreurs.",
  attachments: [
    {
      filename: 'cadreco-api.html',
      path: 'newman/cadreco-api.html',
      cid: 'cadreco-api.html' 
    }
  ]
};


const deleteFile = (directory) => {

  fs.readdir(directory, (err, files) => {
    if (err) throw err;
  
    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });

};

app.get('/', function (req, res) {
    console.log('----------Start---------------');

    newman.run({
        collection: require('./collection/CADRECO-API.postman_collection.json'),
        reporters: ['htmlextra'],
        reporter: {
          htmlextra: {
              export: './newman/cadreco-api.html',
          }
        },
        environment: {
          "values": [
            {
                "key": "baseUrl",
                "value": "localhost:3000",
                "enabled": true
            },
            {
              "key": "version",
              "value": "v1",
              "enabled": true
            }
          ],
        }
    }).on('start', function (err, args) { // on start of run, log to console
      console.log('running a collection...');
    }).on('done', async (err, summary) => {
        if (err || summary.error || summary.run.failures.length > 30) {
          transporter.sendMail(mailOptions, (err, data) => {
            if (err) {
                return console.log('Error occurs');
            }
            deleteFile(directory);
            return console.log('Email sent!!!');
          });
          
          console.error('collection run encountered an error.'+summary.run.failures.length);     
        }
        else {
          deleteFile(directory);
          console.log('collection run completed.');
        }
        
    });

  res.send('Test finish');
});

app.listen(3001, function () {
  console.log('listening on port 3001!');
});